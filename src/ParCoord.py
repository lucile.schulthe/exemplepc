# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 09:02:38 2024

@author: lucile.schulthe
"""


import plotly.express as px
import pandas as pd
import plotly.graph_objects as go

pd.DataFrame.iteritems = pd.DataFrame.items

# Sample data

data = pd.read_csv("../data/database.csv",encoding='ISO-8859-1')
df=pd.DataFrame(data)


# %% With plotly express

dims = ['Facade orientation','Wall structure type','Facade cladding type','Wall insulation type','OI','EI','Facade EI', 'sDA','Glazing area']

plotly_fig = px.parallel_coordinates(df, color=df['Glazing area'],
                              dimensions=dims)


plotly_fig.write_html("graphique_parallel_coordinates_withplotly.html")

# Ouvrez le fichier HTML dans votre navigateur
import webbrowser
webbrowser.open("graphique_parallel_coordinates_withplotly.html", new=2)

# %% With graph objects

dimswithname=list([
            dict(label = 'Orientation', tickvals = [1,2,3,4], ticktext = ['S', 'N-S', 'N-S-W', 'N-S-W-E'], values = df['Facade orientation']),
            dict(label = 'Wall_str', tickvals = [1,2,3,4], ticktext = ['Concrete', 'Brick', 'MassiveWood', 'WoodFramed'], values = df['Wall structure type']),
            dict(label = 'Fac_clad', tickvals = [1,2,3,4], ticktext = ['Min_plast', 'Cem_plast', 'Wood', 'Fibrocement'], values = df['Facade cladding type']),
            dict(label = 'Wall_ins', tickvals = [1,2,3,4], ticktext = ['Straw', 'Cellulose', 'Glass wool', 'EPS'], values = df['Wall insulation type']),
            dict(label = 'OI',  values = df['OI']),
            dict(label = 'EI',  values = df['EI']),
            dict(label = 'Facade EI',  values = df['Facade EI']),
            dict(label = 'sDA',  values = df['sDA']),
            dict(label = 'Glazing area',range = [min(df['Glazing area']),max(df['Glazing area'])],  values = df['Glazing area']),
            ])


fig=go.Figure(go.Parcoords(
    line=dict(color=df['Glazing area'], showscale=True),
    dimensions=dimswithname))

# %%
fig.write_html("graphique_parallel_coordinates_withobjects.html")

    # Ouvrez le fichier HTML dans votre navigateur
import webbrowser
webbrowser.open("graphique_parallel_coordinates_withobjects.html", new=2)